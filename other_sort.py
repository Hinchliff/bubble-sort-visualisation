import sys
import pygame
import random
import time
from pygame.locals import *


def generate_array(number, maximum):
    mixedup = []
    for i in range(0, number):
        mixedup.append(random.randint(1, maximum))
    return mixedup


def generate_boxes(screen, font, mixedup):
    l = len(mixedup)
    m = max(mixedup)
    w, h = pygame.display.get_surface().get_size()
    scale = m/(h-(h*0.2))
    for i, val in enumerate(mixedup):
        pygame.draw.rect(screen, [0,0,255], ((w/(l+2))*(i+1), h-(h*0.1), w/(l+2), -val/scale), 0)
        pygame.draw.rect(screen, [0,0,0], ((w/(l+2))*(i+1), h-(h*0.1), w/(l+2), -val/scale), 1)
        text = font.render(str(val), False, (0, 0, 0))
        screen.blit(text, ((w/(l+2))*(i+1), h-(h*0.075)))


def check_order(mixedup):
    for i in range(0, len(mixedup)-1):
        if (mixedup[i] > mixedup[i+1]):
            return False
    return True


def bubble_sort(screen, font, mixedup):
    finished = False
    l = len(mixedup)
    m = max(mixedup)
    w, h = pygame.display.get_surface().get_size()
    scale = m/(h-(h*0.2))
    p = 0
    j = 0
    mode = 0
    change = 0
    screen.fill([255,255,255])
    generate_boxes(screen, font, mixedup)
    while not finished:
        temp = 0
        if change == 1:
            screen.fill([255,255,255])
            generate_boxes(screen, font, mixedup)
            pygame.display.update()
            change = 0

        if (p + 1) >= l:
            p = 0
            finished = check_order(mixedup)
        elif (p - 1) < 0:
            mode = 0

        #Are we moving up or down?
        if mode == 0:
            if p + 1 < l and mixedup[p] > mixedup[p+1]:
                #We swap with the next element up the list
                temp = mixedup[p]
                mixedup[p] = mixedup[p+1]
                mixedup[p+1] = temp
                # We have swapped so we change directions
                mode = 1
                change = 1
                j = p
            else:
                # We use green to show we are happy with our order
                pygame.draw.rect(screen, [0,255,0], ((w/(l+2))*(p+1), h-(h*0.1), w/(l+2), -mixedup[p]/scale), 0)
                pygame.draw.rect(screen, [0,0,0], ((w/(l+2))*(p+1), h-(h*0.1), w/(l+2), -mixedup[p]/scale), 1)
                p+=1
        elif mode == 1:
            if p - 1 > -1 and mixedup[p] < mixedup[p-1]:
                #We swap with the variable below
                temp = mixedup[p]
                mixedup[p] = mixedup[p-1]
                mixedup[p-1] = temp
                #We keep going untill we find something in order
                pygame.draw.rect(screen, [255,0,0], ((w/(l+2))*(p+1), h-(h*0.1), w/(l+2), -mixedup[p-1]/scale), 0)
                pygame.draw.rect(screen, [0,0,0], ((w/(l+2))*(p+1), h-(h*0.1), w/(l+2), -mixedup[p-1]/scale), 1)
                p-=1
                change = 1
            else:
                mode = 0
                change = 1
                p = j
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
    print("Finished!")
    screen.fill([255,255,255])
    generate_boxes(screen, font, mixedup)
    pygame.display.update()


def main():
    number = 20
    maximum = 30
    q = False
    if len(sys.argv) > 2:
        number = int(sys.argv[1])
        maximum = int(sys.argv[2])
    if len(sys.argv) == 4:
        if sys.argv[3] == "q":
            q = True
        else:
            print("Invalid arguments, format: Number Range q")

    pygame.init()
    screen = pygame.display.set_mode((960, 540))
    pygame.display.set_caption('Bubble Sort')
    screen.fill([255, 255, 255])
    pygame.font.init()
    font = pygame.font.SysFont('Inconsolata-Regular', int(pygame.display.get_surface().get_size()[0]/number))

    mixedup = generate_array(int(number), (int(maximum)))
    print(mixedup)
    bubble_sort(screen, font, mixedup)
    print(mixedup)
    while not q:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()



if __name__ == "__main__":
    main()
