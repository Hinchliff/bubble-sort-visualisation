import sys
import pygame
import random
import time
from pygame.locals import *


def generate_array(number, maximum):
    mixedup = []
    for i in range(0, number):
        mixedup.append(random.randint(1, maximum))
    return mixedup


def generate_boxes(screen, font, mixedup, p):
    l = len(mixedup)
    m = max(mixedup)
    w, h = pygame.display.get_surface().get_size()
    scale = m/(h-(h*0.2))
    for i, val in enumerate(mixedup):
        if i == p:
            pygame.draw.rect(screen, [0,255,255], ((w/(l+2))*(i+1), h-(h*0.1), w/(l+2), -val/scale), 0)
            pygame.draw.rect(screen, [0,0,0], ((w/(l+2))*(i+1), h-(h*0.1), w/(l+2), -val/scale), 1)
        else:
            pygame.draw.rect(screen, [0,0,255], ((w/(l+2))*(i+1), h-(h*0.1), w/(l+2), -val/scale), 0)
            pygame.draw.rect(screen, [0,0,0], ((w/(l+2))*(i+1), h-(h*0.1), w/(l+2), -val/scale), 1)
        text = font.render(str(val), False, (0, 0, 0))
        screen.blit(text, ((w/(l+2))*(i+1), h-(h*0.075)))


def check_order(mixedup):
    for i in range(0, len(mixedup)-1):
        if (mixedup[i] > mixedup[i+1]):
            return False
    return True


def bubble_sort(screen, font, mixedup):
    finished = False
    l = len(mixedup)
    m = max(mixedup)
    w, h = pygame.display.get_surface().get_size()
    scale = m/(h-(h*0.2))
    p = l
    change = 0
    screen.fill([255,255,255])
    generate_boxes(screen, font, mixedup, -1)
    while not finished:
        temp = 0

        if p == 0:
            finished = check_order(mixedup)

        for i in range(0, p):
            if change == 1:
                screen.fill([255,255,255])
                generate_boxes(screen, font, mixedup, p)
                pygame.display.update()
                change = 0

            if i + 1 < p and mixedup[i] > mixedup[i+1]:
                temp = mixedup[i]
                mixedup[i] = mixedup[i+1]
                mixedup[i+1] = temp
                pygame.draw.rect(screen, [255,0,0], ((w/(l+2))*(i+1), h-(h*0.1), w/(l+2), -mixedup[i+1]/scale), 0)
                pygame.draw.rect(screen, [0,0,0], ((w/(l+2))*(i+1), h-(h*0.1), w/(l+2), -mixedup[i+1]/scale), 1)
                pygame.display.update();
                change = 1
            else:
                pygame.draw.rect(screen, [0,255,0], ((w/(l+2))*(i), h-(h*0.1), w/(l+2), -mixedup[i]/scale), 0)
                pygame.draw.rect(screen, [0,0,0], ((w/(l+2))*(i), h-(h*0.1), w/(l+2), -mixedup[i]/scale), 1)
                change = 1
        p -= 1

        pygame.display.update()
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

    print("Finished!")
    screen.fill([255,255,255])
    generate_boxes(screen, font, mixedup, -1)
    pygame.display.update()


def main():
    number = 20
    maximum = 30
    q = False
    if len(sys.argv) > 2:
        number = int(sys.argv[1])
        maximum = int(sys.argv[2])
    if len(sys.argv) == 4:
        if sys.argv[3] == "q":
            q = True
        else:
            print("Invalid arguments, format: Number Range q")

    pygame.init()
    screen = pygame.display.set_mode((960, 540))
    pygame.display.set_caption('Bubble Sort')
    screen.fill([255, 255, 255])
    pygame.font.init()
    font = pygame.font.SysFont('Inconsolata-Regular', int(pygame.display.get_surface().get_size()[0]/number))

    mixedup = generate_array(int(number), (int(maximum)))
    print(mixedup)
    bubble_sort(screen, font, mixedup)
    print(mixedup)
    while not q:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()



if __name__ == "__main__":
    main()
